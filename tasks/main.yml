---
- name: Installiere Pakete
  apt: "name={{ item }} update_cache=yes cache_valid_time=86400"
  with_items: apache_packages
  tags:
    - apache

- name: Konfigurere ports.conf
  template: >
    src={{ item }}
    dest=/etc/apache2/ports.conf
  with_first_found:
  - "../templates/{{ ansible_distribution }}-{{ ansible_distribution_version }}/ports.conf.j2"
  - "../templates/{{ ansible_distribution }}/ports.conf.j2"
  - "../templates/{{ ansible_os_family }}/ports.conf.j2"
  - "../templates/ports.conf.j2"
  notify:
   - reload apache2
  tags:
    - apache

- name: Konfiguriere SSL-Modul
  lineinfile:
    dest: /etc/apache2/mods-available/ssl.conf
    regexp: "^\\s*{{ item.option }}"
    line: "        {{ item.option }} {{ item.value }}"
  with_items:
    - option: SSLProtocol
      value: "{{ apache_module_ssl_protocol }}"
    - option: SSLCipherSuite
      value: "{{ apache_module_ssl_cipher_suite }}"
  notify:
   - restart apache2
  tags:
    - apache

- name: Konfiguriere fcgid-Modul MaxRequestLen
  lineinfile:
    dest: /etc/apache2/mods-available/fcgid.conf
    insertafter: \<IfModule mod_fcgid\.c\>
    regexp: "^\\s*MaxRequestLen"
    line: "  MaxRequestLen {{ apache_module_fcgid_max_request_len }}"
  notify:
   - restart apache2
  when: apache_module_fcgid_max_request_len != False
  tags:
    - apache

- name: Konfiguriere mpm_prefork-Modul MaxRequestWorkers
  lineinfile:
    dest: /etc/apache2/mods-available/mpm_prefork.conf
    insertafter: \<IfModule mpm_prefork_module\>
    regexp: "^\\s*MaxRequestWorkers"
    line: "  MaxRequestWorkers {{ apache_module_mpm_prefork_max_request_workers }}"
  notify:
   - restart apache2
  tags:
    - apache

- name: Aktiviere gewünschte Module
  apache2_module: "name={{ item }}"
  with_items: apache_module_enable
  notify:
   - restart apache2
  tags:
    - apache

- name: Deaktiviere gewünschte Module
  apache2_module: "name={{ item }} state=absent"
  with_items: apache_module_disable
  notify:
   - restart apache2
  tags:
    - apache

- name: Aktiviere gewünschte Seiten
  shell: "a2ensite {{ item }}"
  register: apache_cmd
  changed_when: apache_cmd.stdout.endswith(' already enabled') == false
  with_items: apache_site_enable
  notify:
   - reload apache2
  tags:
    - apache

- name: Deaktiviere gewünschte Seiten
  shell: "a2dissite {{ item }}"
  register: apache_cmd
  changed_when: apache_cmd.stdout.endswith(' already disabled') == false
  with_items: apache_site_disable
  notify:
   - reload apache2
  tags:
    - apache

- name: Konfiguriere Logrotate für Apache-Logs
  template:
    src: "{{ item }}"
    dest: /etc/logrotate.d/apache2
  with_first_found:
  - "../templates/{{ ansible_distribution }}-{{ ansible_distribution_version }}/logrotate.j2"
  - "../templates/{{ ansible_distribution }}/logrotate.j2"
  - "../templates/{{ ansible_os_family }}/logrotate.j2"
  - "../templates/logrotate.j2"
  tags:
    - apache
